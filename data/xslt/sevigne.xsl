<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="ref"/>

    <xsl:include href="common.xsl"/>
    <xsl:include href="credits.xsl"/>
    <xsl:include href="bootstrap_rendering.xsl"/>
    <xsl:include href="bib.xsl"/>

    <xsl:template match="tei:teiHeader"/>

    <xsl:template match="tei:teiCorpus">
        <xsl:variable name="corpusTitle">
            <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
        </xsl:variable>
        <xsl:message>Generating page: ../views/project.html.twig</xsl:message>
        <xsl:result-document href="../views/project.html.twig" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <xsl:text>{% extends "base.html.twig" %}</xsl:text>
            <xsl:value-of select="concat($br, $br)"/>
            <xsl:text>{% block title %}</xsl:text>
            <xsl:value-of select="$corpusTitle"/>
            <xsl:text>{% endblock%}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block navstyle %}background-size: cover; background-image: url('img/papierpeint.png');{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block navlogo %}data/img/medaillon_fondTransparent.png{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block tei_link %}./data/tei/</xsl:text>
            <xsl:value-of select="replace(base-uri(.), '.*/', '')"/>
            <xsl:text>{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block favicon %}data/img/medaillon_fondTransparent.png{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block legalpage %}./index.php?page=credits{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block menu %}</xsl:text>
            <xsl:call-template name="block_menu"/>
            <xsl:text>{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block logos %}</xsl:text>
            <xsl:call-template name="block_logos"/>
            <xsl:text>{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block sourcelink %}</xsl:text>
            <xsl:call-template name="block_sourcelink"/>
            <xsl:text>{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
        </xsl:result-document>

        <xsl:message>Generating page: ../views/credits.html.twig</xsl:message>
        <xsl:result-document href="../views/credits.html.twig" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <xsl:text>{% extends "project.html.twig" %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block title %}</xsl:text>
            <xsl:value-of select="$corpusTitle"/>
            <xsl:text> - Crédits et mentions légales</xsl:text>
            <xsl:text>{% endblock %}</xsl:text>
            <xsl:value-of select="$br"/>
            <xsl:text>{% block content %}</xsl:text>
            <div class="row">
                <div class="col text-justify">
                    <xsl:call-template name="publication_edition_source"/>
                    <xsl:call-template name="credits_figure"/>
                    <xsl:call-template name="people_involved"/>
                </div>
            </div>
            <xsl:text>{% endblock %}</xsl:text>
        </xsl:result-document>

        <xsl:for-each select="tei:TEI">
            <xsl:message>Generating page: <xsl:value-of select="@xml:id"/></xsl:message>
            <xsl:variable name="page">
                <xsl:value-of select="@xml:id"/>
            </xsl:variable>
            <xsl:variable name="href">
                <xsl:text>../views/</xsl:text>
                <xsl:value-of select="$page"/>
                <xsl:text>.html.twig</xsl:text>
            </xsl:variable>
            <!-- INDEX -->
            <xsl:result-document href="{$href}" method="xhtml" indent="yes"
                omit-xml-declaration="yes">
                <xsl:text>{% extends "project.html.twig" %}</xsl:text>
                <xsl:text>{% block title %}</xsl:text>
                <xsl:value-of select="$corpusTitle"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
                <xsl:text>{% endblock %}

            {% block tei_link %}./data/tei/</xsl:text>
                <xsl:value-of select="replace(base-uri(.), '.*/', '')"/>
                <xsl:text>{% endblock %}

            {% block content %}
            </xsl:text>
                <div class="row">
                    <div class="col text-justify">
                        <xsl:apply-templates select="tei:text/tei:body"/>
                    </div>
                </div>
                <xsl:text>{% endblock %}</xsl:text>
            </xsl:result-document>
            <xsl:message> See: <xsl:value-of select="$href"/></xsl:message>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:body">
        <xsl:choose>
            <xsl:when test="count(./tei:figure) > 1">
                <xsl:call-template name="create_carousel"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="./tei:figure" mode="unique"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="count(tei:div[@type = 'section']) > 1">
            <!--<xsl:call-template name="create_nav_tabs"/>-->
            <xsl:call-template name="create_sub_menu"/>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(tei:div[@type = 'section']) > 1">
                <!--<div class="tab-content">-->
                <xsl:apply-templates/>
                <!--</div>-->
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:head">
        <xsl:choose>
            <xsl:when test="ancestor::tei:TEI/@xml:id = 'index'">
                <H1>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </H1>
            </xsl:when>
            <xsl:otherwise>
                <H2>
                    <xsl:attribute name="id">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </H2>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
            <xsl:if test="not(substring(., string-length(.)) = '.')">
                <xsl:text>.</xsl:text>
            </xsl:if>
        </p>
    </xsl:template>

    <xsl:template match="tei:list">
        <ul>
            <xsl:for-each select="tei:item">
                <li>
                    <xsl:attribute name="class">
                        <xsl:text>text-</xsl:text>
                        <xsl:value-of select="@rendition"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="tei:list[@type = 'gallery']">
        <div class="row">
            <div class="gallery" id="gallery">
                <xsl:for-each select=".//tei:figure">
                    <!--col-3 mr-2 mb-2-->
                    <div class="card mb-3 pics animation all 2" type="button" data-toggle="modal">
                        <xsl:attribute name="data-target">
                            <xsl:text>#modal</xsl:text>
                            <xsl:value-of select="position()"/>
                        </xsl:attribute>
                        <img>
                            <xsl:attribute name="class">card-img-top img-fluid</xsl:attribute>
                            <xsl:attribute name="alt">
                                <xsl:value-of select="tei:head"/>
                            </xsl:attribute>
                            <xsl:attribute name="src">
                                <xsl:value-of select="tei:graphic/@url"/>
                            </xsl:attribute>
                        </img>
                    </div>
                </xsl:for-each>
            </div>
        </div>
        <xsl:for-each select=".//tei:figure">
            <div class="modal fade" tabindex="-1" role="dialog">
                <xsl:attribute name="id">
                    <xsl:text>modal</xsl:text>
                    <xsl:value-of select="position()"/>
                </xsl:attribute>
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">
                                <xsl:value-of select="tei:head"/>
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                                <i class="fa fa-window-close" aria-hidden="true"/>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                <xsl:value-of select="tei:figDesc"/>
                            </p>
                            <p>
                                <xsl:choose>
                                    <xsl:when test="tei:note[@type = 'rights']">
                                        <xsl:text>Droits : </xsl:text>
                                        <xsl:value-of select="tei:note[@type = 'rights']"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>Droits : </xsl:text>
                                        <i>
                                            <xsl:text>Information absente.</xsl:text>
                                        </i>
                                        <xsl:message> Warning: a tei:note[@type = 'rights'] is
                                            missing. Check the gallery</xsl:message>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="tei:graphic/@facs">
                                    <xsl:text> - Source : </xsl:text>
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="tei:graphic/@facs"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="tei:graphic/@facs"/>
                                    </a>
                                </xsl:if>
                            </p>
                            <img width="50%">
                                <xsl:attribute name="alt">
                                    <xsl:value-of select="tei:head"/>
                                </xsl:attribute>
                                <xsl:attribute name="src">
                                    <xsl:value-of select="tei:graphic/@url"/>
                                </xsl:attribute>
                            </img>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:listBibl">
        <ol class="bibl">
            <xsl:for-each select="tei:bibl">
                <xsl:sort select="(./tei:author[1]/tei:surname) or (./tei:author[1]/text())"
                    order="ascending" data-type="text" lang="french"/>
                <xsl:apply-templates select="." mode="do"/>
            </xsl:for-each>
        </ol>
    </xsl:template>

    <xsl:template match="tei:bibl"/>

    <xsl:template match="tei:bibl" mode="do">
        <xsl:choose>
            <xsl:when
                test="@type = 'link' or @type = 'dig' or @type = 'cat' or @type = 'biblOfBibl'">
                <li>
                    <xsl:choose>
                        <xsl:when test="@type = 'cat'">
                            <xsl:text>Catalogue</xsl:text>
                            <xsl:if test="count(tei:ref) > 1">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                            <xsl:text> :</xsl:text>
                        </xsl:when>
                        <xsl:when test="@type = 'dig'">
                            <xsl:text>Version</xsl:text>
                            <xsl:if test="count(tei:ref) > 1">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                            <xsl:text> numérique</xsl:text>
                            <xsl:if test="count(tei:ref) > 1">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                            <xsl:text> :</xsl:text>
                        </xsl:when>
                        <xsl:when test="@type = 'link'">
                            <xsl:text>Lien</xsl:text>
                            <xsl:if test="count(tei:ref) > 1">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                            <xsl:text> :</xsl:text>
                        </xsl:when>
                        <xsl:when test="@type = 'biblOfBibl'">
                            <xsl:text>Référence</xsl:text>
                            <xsl:if test="count(tei:ref) > 1">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                            <xsl:text> bibliographique</xsl:text>
                            <xsl:if test="count(tei:ref) > 1">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                            <xsl:text> : </xsl:text>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:apply-templates/>
                </li>
            </xsl:when>
            <xsl:when test="@type = 'doc'">
                <li>
                    <xsl:apply-templates/>
                </li>
            </xsl:when>
            <xsl:when test="exists(@type)">
                <li class="fa fa-question">
                    <xsl:apply-templates/>
                </li>
            </xsl:when>
            <xsl:otherwise>
                <li>
                    <xsl:apply-templates/>
                    <xsl:if test="tei:bibl[@type = 'digitised']">
                        <xsl:text> </xsl:text>
                        <a class="fa fa-link">
                            <xsl:attribute name="href">
                                <xsl:value-of select="tei:bibl/tei:ref/@target"/>
                            </xsl:attribute>
                            <xsl:value-of select="tei:bibl/tei:ref"/>
                        </a>
                    </xsl:if>
                    <xsl:if test="exists(tei:note)">
                        <ul class="bib_note">
                            <xsl:for-each select="tei:note">
                                <xsl:apply-templates mode="do" select="."/>
                            </xsl:for-each>
                        </ul>
                    </xsl:if>
                    <xsl:if test="exists(tei:bibl)">
                        <ul class="bib_bibl">
                            <xsl:for-each select="tei:bibl">
                                <xsl:apply-templates mode="do" select="."/>
                            </xsl:for-each>
                        </ul>
                    </xsl:if>
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:surname">
        <!-- Lower case all + Capitalize -->
        <span class="smallcaps">
            <xsl:value-of
                select="concat(upper-case(substring(., 1, 1)), lower-case(substring(., 2)))"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:forename">
        <!-- Upper-case first char and respect the case for the other (in case of two-parts names -->
        <xsl:value-of select="concat(upper-case(substring(., 1, 1)), substring(., 2))"/>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'a']">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'm']">
        <i>
            <xsl:value-of select="."/>
        </i>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'j']">
        <i>
            <xsl:value-of select="."/>
        </i>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'u']">
        <i>
            <xsl:value-of select="."/>
        </i>
    </xsl:template>

    <xsl:template match="tei:note"/>

    <xsl:template match="tei:note" mode="do">
        <xsl:choose>
            <xsl:when test="@type = 'comm'">
                <li>
                    <xsl:apply-templates/>
                </li>
            </xsl:when>
            <xsl:when test="@type = 'reprint'">
                <li>
                    <xsl:text>Réimpression(s) : </xsl:text>
                    <xsl:apply-templates/>
                </li>
            </xsl:when>
            <xsl:otherwise>
                <li>
                    <xsl:message>Warning: type de note inconnu "<xsl:value-of select="@type"
                        />"</xsl:message>
                    <mark style="background-color: orange;" title="Type de note inconnu">
                        <xsl:value-of select="@type"/>
                    </mark>
                    <xsl:apply-templates/>
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:table">
        <table class="table table-hover table-sm">
            <xsl:if test="exists(tei:row[@role = 'label'])">
                <xsl:apply-templates select="tei:row[@role = 'label']"/>
            </xsl:if>
            <tbody>
                <xsl:apply-templates select="tei:row[@role != 'label']"/>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="tei:row[@role = 'label']">
        <thead>
            <tr>
                <xsl:for-each select="tei:cell">
                    <th scope="col">
                        <xsl:apply-templates/>
                    </th>
                </xsl:for-each>
            </tr>
        </thead>
    </xsl:template>

    <xsl:template match="tei:row">
        <tr>
            <xsl:apply-templates/>
        </tr>
    </xsl:template>

    <xsl:template match="tei:cell">
        <td>
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="hi">
        <xsl:choose>
            <xsl:when test="@rend = 'italics'">
                <i>
                    <xsl:apply-templates/>
                </i>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:listEvent">
        <div class="col">
            <xsl:for-each select="tei:event">
                <xsl:sort order="descending" select=".//tei:date/@when | .//tei:date/@from"/>
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="tei:event">
        <div class="row m-2 pb-3 pt-4 border-bottom">
          <div class="col-3">
	    <xsl:choose>
	      <xsl:when test="exists(@facs)">
		<img class="event rounded mt-2" width="99%">
                  <xsl:attribute name="src">
                    <xsl:text>data/img/</xsl:text>
                    <xsl:value-of select="substring-after(@facs, '/')"/>
                  </xsl:attribute>
                  <xsl:if test="exists(tei:label)">
		    <figcaption class="text-secondary">
                      <xsl:value-of select="tei:label"/>
                    </figcaption>
		  </xsl:if>
		</img>
	      </xsl:when>
	      <xsl:when test="exists(tei:label)">
		<figcaption class="text-secondary">
                  <xsl:value-of select="tei:label"/>
                </figcaption>
	      </xsl:when>
	    </xsl:choose>
            </div>
            <div class="col-9">
                <div class="row">
                    <xsl:choose>
                        <xsl:when test="exists(tei:desc/tei:placeName)">
                            <div>
                                <xsl:attribute name="class">
                                    <xsl:text>col-6 fa fa-map-marker</xsl:text>
                                </xsl:attribute>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="tei:desc/tei:placeName"/>
                            </div>
                        </xsl:when>
                        <xsl:when test="exists(tei:desc/tei:orgName)">
                            <div>
                                <xsl:attribute name="class">
                                    <xsl:text>col-6 fa fa-bank</xsl:text>
                                </xsl:attribute>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="tei:desc/tei:orgName"/>
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div>
                                <xsl:attribute name="class">
                                    <xsl:text>col-6</xsl:text>
                                </xsl:attribute>
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>

                    <div class="fa fa-calendar col-6 text-right">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="tei:desc/tei:date"/>
                    </div>
                </div>
                <p>
                    <xsl:apply-templates mode="event"/>
                </p>
                <xsl:if test="exists(tei:listBibl)">
                    <div class="row">
                        <div class="col-12">
                            <p class=" fa fa-book"> Bibliographie associée :</p>
                            <xsl:apply-templates select="tei:listBibl"/>
                        </div>
                    </div>
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="tei:label" mode="event"/>

    <xsl:template match="tei:placeName" mode="event"/>
    <xsl:template match="tei:orgName" mode="event"/>

    <xsl:template match="tei:date" mode="event"/>

    <xsl:template match="tei:listBibl" mode="event"/>
    
    <xsl:template match="tei:list[@type = 'academic']">
        <h3 class="fa fa-bank">
            <xsl:text> </xsl:text>
            <xsl:value-of select="tei:head"/>
        </h3>
        <ul>
            <xsl:apply-templates select="tei:item"/>
        </ul>
    </xsl:template>

    <xsl:template match="tei:list[@type = 'places']">
        <h3 class="fa fa-map-marker">
            <xsl:text> </xsl:text>
            <xsl:value-of select="tei:head"/>
        </h3>
        <ul>
            <xsl:apply-templates select="tei:item"/>
        </ul>
    </xsl:template>

    <xsl:template match="tei:item">
        <li>
            <xsl:value-of select="tei:label"/>
            <ul>
                <li>
                    <xsl:value-of select="tei:desc"/>
                </li>
                <li>
                    <a class="fa fa-external-link">
                        <xsl:attribute name="href">
                            <xsl:value-of select="tei:ref/@target"/>
                        </xsl:attribute>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="normalize-space(tei:ref)"/>
                    </a>
                </li>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="tei:ref">
        <a>
            <xsl:attribute name="class">
                <xsl:text>fa </xsl:text>
                <xsl:choose>
                    <xsl:when test="parent::tei:bibl/@type = 'doc'">
                        <xsl:text>fa-archive</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>fa-link</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="href">
                <xsl:value-of select="./@target"/>
            </xsl:attribute>
            <xsl:text> </xsl:text>
            <xsl:value-of select="."/>
        </a>
        <xsl:text> </xsl:text>
    </xsl:template>

    <xsl:template match="tei:listPerson">
        <div class="col">
            <xsl:for-each select="tei:person">
                <xsl:choose>
                    <xsl:when test="(position() mod 2) = 0">
                        <xsl:apply-templates select=".">
                            <xsl:with-param name="mode">right</xsl:with-param>
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select=".">
                            <xsl:with-param name="mode">left</xsl:with-param>
                        </xsl:apply-templates>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="tei:person">
        <xsl:param name="mode"/>
        <div class="row m-2 pb-3 pt-4 border-bottom person-card">
            <div>
                <xsl:attribute name="class">
                    <xsl:text>col-3 order-</xsl:text>
                    <xsl:choose>
                        <xsl:when test="$mode = 'left'">1</xsl:when>
                        <xsl:when test="$mode = 'right'">2</xsl:when>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="exists(@facs)">
                        <img class="person rounded mt-2" width="99%">
                            <xsl:attribute name="src">
                                <xsl:text>data/img/</xsl:text>
                                <xsl:value-of select="substring-after(@facs, '/')"/>
                            </xsl:attribute>
                        </img>
                    </xsl:when>
                    <xsl:when test="exists(@corresp)">
                        <img class="person rounded mt-2" width="99%">
                            <xsl:attribute name="src">
                                <xsl:value-of select="@corresp"/>
                            </xsl:attribute>
                        </img>
                    </xsl:when>
                </xsl:choose>
            </div>
            <div>
                <xsl:attribute name="class">
                    <xsl:text>col-9 order-</xsl:text>
                    <xsl:choose>
                        <xsl:when test="$mode = 'left'">2</xsl:when>
                        <xsl:when test="$mode = 'right'">1</xsl:when>
                    </xsl:choose>
                </xsl:attribute>

                <div class="row">
                    <div class="col-6 offset-6 text-right">
                        <xsl:if test="exists(tei:bibl)">
                            <a class="fa fa-external-link">
                                <xsl:attribute name="href">
                                    <xsl:value-of select="tei:bibl/tei:ref/@target"/>
                                </xsl:attribute>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="tei:bibl/tei:ref"/>
                            </a>
                        </xsl:if>
                    </div>
                    <h3>
                        <xsl:apply-templates select="tei:persName"/>
                    </h3>
                    <p>
                        <i>
                            <xsl:value-of select="tei:occupation"/>
                        </i>
                    </p>
                    <p>
                        <xsl:value-of select="tei:note"/>
                    </p>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="tei:figure" mode="unique">
        <img>
            <xsl:attribute name="class">rounded float-right img-thumbnail p-2 ml-3 mb-3</xsl:attribute>
            <xsl:attribute name="style">background:transparent; max-height: calc(100vh - 180px);</xsl:attribute>
            <xsl:attribute name="src">
                <xsl:value-of select="@facs"/>
            </xsl:attribute>
        </img>
    </xsl:template>

    <xsl:template match="tei:abbr">
        <xsl:choose>
            <xsl:when test="@type = 'siglum'">
                <mark style="background-color: orange;" title="siglum">[<xsl:value-of select="."
                    />]</mark>
            </xsl:when>
            <xsl:otherwise>
                <mark style="background-color: pink;" title="siglum">[<xsl:value-of select="."
                    />]</mark>
                <xsl:message>Warning: abbr with type "<xsl:value-of select="@type"/>"</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:div[@type = 'section']">
        <!--<div>
            <xsl:attribute name="class">
                <xsl:text>tab tab-pane fade</xsl:text>
                <xsl:if test="not(exists(preceding-sibling::tei:div[@type = 'section']))">
                    <xsl:text> active show</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <xsl:attribute name="id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>-->
        <div>
            <xsl:attribute name="id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:div">
        <div>
            <xsl:if test="exists(@xml:id)">
                <xsl:attribute name="id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:link">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="@target"/>
            </xsl:attribute>
            <xsl:value-of select="@target"/>
        </a>
    </xsl:template>

</xsl:stylesheet>
