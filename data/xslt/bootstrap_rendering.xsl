<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>


    <xsl:template name="create_nav_tabs">
        <nav class="navbar navbar-expand navbar-light nav-tabs mx-auto">
            <ul class="navbar-nav">
                <xsl:for-each select="tei:div[@type = 'section']">
                    <li>
                        <xsl:attribute name="class">
                            <xsl:text>nav-item</xsl:text>
                            <xsl:if test="position() = 1">
                                <xsl:text> active</xsl:text>
                            </xsl:if>
                        </xsl:attribute>
                        <a class="nav-link" data-toggle="tab">
                            <xsl:attribute name="href">
                                <xsl:text>#</xsl:text>
                                <xsl:value-of select="@xml:id"/>
                            </xsl:attribute>
                            <xsl:value-of select="./descendant::tei:head[1]/text()"/>
                        </a>
                    </li>
                </xsl:for-each>
            </ul>
        </nav>
    </xsl:template>

    <xsl:template name="create_sub_menu">
        <div id="sidebar" class="col-2 d-md-block sidebar pr-3">
            <div class="sidebar-sticky">
                <ul class="nav navbar-light flex-column ">
                    <xsl:for-each select="tei:div[@type = 'section']">
                        <li class="navbar-nav nav-item ml-4">
                            <h5>
                                <xsl:variable name="id_page">
                                    <xsl:value-of select="./ancestor::tei:TEI/@xml:id"/>
                                </xsl:variable>
                                <a class="nav-link" href="?page={$id_page}#{@xml:id}">
                                    <xsl:value-of select="./descendant::tei:head[1]/text()"/>
                                </a>
                            </h5>
                        </li>
                    </xsl:for-each>
                </ul>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="create_carousel">
        <div id="carouselExampleIndicators" class="carousel slide float-right ml-3"
            data-ride="carousel">
            <ol class="carousel-indicators">
                <xsl:for-each select="./tei:figure">
                    <xsl:sort select="@n" order="ascending"/>
                    <li data-target="#carouselExampleIndicators">
                        <xsl:attribute name="data-slide-to">
                            <xsl:value-of select="position()"/>
                        </xsl:attribute>
                        <xsl:if test="position() = 1">
                            <xsl:attribute name="class">active</xsl:attribute>
                        </xsl:if>
                    </li>
                </xsl:for-each>
            </ol>
            <div class="carousel-inner">
                <xsl:attribute name="style">
                    <xsl:text>width: 300px;</xsl:text>
                </xsl:attribute>
                <xsl:for-each select="./tei:figure">
                    <xsl:sort select="@n" order="ascending"/>
                    <div>
                        <xsl:attribute name="class">
                            <xsl:text>carousel-item</xsl:text>
                            <xsl:if test="position() = 1"> active</xsl:if>
                        </xsl:attribute>
                        <img class="d-block rounded">
                            <xsl:attribute name="src">
                                <xsl:value-of select="@facs"/>
                            </xsl:attribute>
                            <xsl:attribute name="style">
                                <xsl:text>width: 300px;</xsl:text>
                            </xsl:attribute>
                        </img>
                    </div>
                </xsl:for-each>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"/>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"/>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </xsl:template>
</xsl:stylesheet>
